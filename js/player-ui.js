var puiPlay
var puiPause
var puiPlaying
var puiTimer
var puiTimerInterval
var puiLastTime

puiTimerInterval = 500
puiPlaying = false
puiPlay = document.querySelector('.player-play')
puiPause = document.querySelector('.player-pause')
puiLoader = document.querySelector('.player-loader')
puiMeta = document.querySelector('.player-meta')

puiPlay.addEventListener('click', function(e){
    puiStart()
})
puiPause.addEventListener('click', function(e){
    puiStop()
})
puiLoader.addEventListener('click', function(e){
    puiStop()
})

function puiStart(){
    puiPlaying = true
    play()
    clearInterval(puiTimer)
    puiTimer = setInterval(function(){
        puiUpdater()
    }, puiTimerInterval)
}
function puiStop(){
    puiPlaying = false
    pause()
    puiUpdater()
    media.removeAttribute('src')
    media.load()
    clearInterval(puiTimer)
}
function puiRestart(){
    pause()
    play()
}

function puiUpdater(){
    console.log('puiUpdater ::: ')
    var currentTime = media.currentTime
    console.log(puiLastTime + puiTimerInterval/2/1000)
    console.log(currentTime)
    if (puiPlaying && (currentTime < puiLastTime + puiTimerInterval/2/1000)) {
        console.log('puiUpdater ::: stream lost, restarting..')
        // puiRestart()
    } else {
        console.log('puiUpdater ::: stream is running')
    }
    puiLastTime = currentTime
    // currentTime = currentTime + 55 + 60 * 59
    // format time 
    var h = parseInt(currentTime/60/60)
    var m = parseInt(currentTime/60 - h*60)
    var s = parseInt(currentTime - m*60 - h*60*60)
    if (h < 10) h = "0" + h
    if (m < 10) m = "0" + m
    if (s < 10) s = "0" + s


    if (puiPlaying && media.readyState >= 3) {
        puiPlay.style.display = "none"
        puiPause.style.display = "block"
        puiLoader.style.display = "none"
        puiMeta.innerHTML = "Time: " + h + ":" + m + ":" + s
    }
    if (puiPlaying && media.readyState < 3) {
        puiPlay.style.display = "none"
        puiPause.style.display = "none"
        puiLoader.style.display = "block"
        puiMeta.innerHTML = "...Loading..."
    }
    if (!puiPlaying) {
        puiPlay.style.display = "block"
        puiPause.style.display = "none"
        puiLoader.style.display = "none"
        puiMeta.innerHTML = ""
    }
    
}

// setInterval(function(){
//     console.log(media.readyState)
// },100)
