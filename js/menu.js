var menu
var menuButton
var menuVisible = true

menu = document.querySelector('.menu')
menuButton = document.querySelector('.menu-mobile-button')
menuButton.addEventListener('click', function(e){
    if (!menuVisible) {
        menuShow()
    } else {
        menuHide()
    }
})


function menuShow(){
    menu.style.display = "block"
    menuVisible = true    
}
function menuHide(){
    menu.style.display = "none"
    menuVisible = false
}
function menuCheck(){
    if (window.innerWidth > 768) {
        menuShow()
    } else {
        menuHide()
    }
}

window.onresize = function(){
    menuCheck()
}

menuCheck()
