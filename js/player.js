var media;
var ltime;
var ctlast;
var ctcount;


function play() {
    // media.load()
    media.play();
}

function pause(){
    media.pause();
}

function log(msg) {

    console.log(msg);
    /*
    var currentdate = new Date(); 
    var datetime = "" + currentdate.getDate() + "/"
        + (currentdate.getMonth()+1)  + "/" 
        + currentdate.getFullYear() + " @ "  
        + currentdate.getHours() + ":"  
        + currentdate.getMinutes() + ":" 
        + currentdate.getSeconds();
    var logp = document.getElementById("log");
    var logtext = logp.innerHTML; 
    logtext += datetime + ': ' + msg + "<br/>";
    logp.innerHTML = logtext;
*/
}

function fixcache() {
    var sources = document.getElementsByTagName("source");
    for (var child = media.firstElementChild; child !== null; child = child.nextElementSibling) {
        //      console.log(child);
        //  console.log(child.getAttribute("src"));
        var src = child.getAttribute("src");
        // var mystring = '<img src="[media id=5]" />';
        //  console.log(src)
        //  var re = /.+nocache="?(\d+)"?\]/gi;
        var re = /(\w+)\?\w+=(\w+)/;
        var fixed = src.replace(re, "$1?nocache=");
        var rstring = Math.random().toString(36).substring(7);
        var newrand = fixed + rstring;

        child.setAttribute("src", newrand);
        //  console.log(child.innerHtml);
    }
}


function fetchbefore() {
    /*
  fetch('http://10.205.25.56/hls/papillon_128k.m3u8')
    .then(function(response) {
          return response.body();
        })
    .then(function(myJson) {
        log(myJson);
})
*/
    return true;
}

function updatemetadata() {

    var infotext = "<h1> Stream info </h1> <ul>";
    for (source in sources) {
        //      console.log(sources[source]);
        var url = sources[source].listenurl;
        if (url == "http://radiopatapoe.nl:8000/papillon-archive.ogg") {
            //         console.log(url + "matched");
            pap = sources[source];
            infotext += "<li>title: " + pap.title + "</li>";
            infotext += "<li>start: " + pap.stream_start + "</li>";
            infotext += "<li>genre: " + pap.genre + "</li>";
            infotext += "<li>name: " + pap.server_name + "</li>";

        }
    }
    infotext += "</ul>";
    info.innerHTML = JSON.stringify(infotext);
}

function updatemetadata() {
    fetch('https://adm.amsterdam/radio/proxy.php?url=http://radiopatapoe.nl:8000/status-json.xsl')
        .then(function(response) {
            return response.json();
        })
        .then(function(myJson) {
            var info = document.getElementById("info");
            var sources = myJson.contents.icestats.source;
            //       console.log(sources);
            var infotext = "<h1> Stream info </h1> <ul>";
            for (source in sources) {
                //      console.log(sources[source]);
                var url = sources[source].listenurl;
                if (url == "http://radiopatapoe.nl:8000/papillon-archive.ogg") {
                    //         console.log(url + "matched");
                    pap = sources[source];
                    infotext += "<li>title: " + pap.title + "</li>";
                    infotext += "<li>start: " + pap.stream_start + "</li>";
                    infotext += "<li>genre: " + pap.genre + "</li>";
                    infotext += "<li>name: " + pap.server_name + "</li>";
                }
            }
            infotext += "</ul>";
            info.innerHTML = JSON.stringify(infotext);
        });
}

function checktimer() {
    //log('timeupdate last:' + ltime + 'current: ' + media.currentTime);
    if (media.currentTime == ctlast) {
        ctcount++
        //log("sametime times:" + ctcount);
    }

    ctlast = media.currentTime;

    if (ctcount > 6) {
        log("not playing for a while, reloading!");
        ctcount = 0;
        fixcache();
        // media.load();
        // media.play();
        //updatemetadata();
    }
}
//audio.addEventListener("seeked", function() { console.log("seeked"); }, true);
//audio.addEventListener("timeupdate", function() { console.log("seeked"); }, true);
//audio.addEventListener("error", function() { console.log("seeked"); }, true);
//audio.addEventListener("ended", function() { console.log("seeked"); }, true);

function checktypes() {
    log("mp4: " + media.canPlayType('video/mp4'));
    log("mpeg: " + media.canPlayType('audio/mpeg'));
    log("mp3: " + media.canPlayType('audio/mp3'));
    log("ogg: " + media.canPlayType('audio/ogg'));
    log("x-mpeg: " + media.canPlayType('application/x-mpegURL'));
    log("apple.mpeg: " + media.canPlayType('application/vnd.apple.mpegurl'));
}

function checkstream() {
    //checktypes();
    //log("check");
    log("state: " + media.currentState);
    log("audiotracks: " + media.audioTracks);
    log("cross: " + media.crossOrigin);
    log("netstate: " + media.networkState);
    log("src: " + media.src);
    log("srcObj: " + media.srcObject);

    checktimer();
    fetchbefore();
}
/*
window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
      log("Error occured: " + errorMsg);//or any message
      return false;
}
*/

function whenLoaded() {
    ltime = 0;
    ctlast = 0;
    ctcount = 0;

    //var audio = document.getElementsByTagName("audio")[0];
    media = document.getElementsByTagName("audio")[0];

    console.log(media);
    media.addEventListener('abort', function() {
        // media.pause();              
        log('abort');
    }, true);

    media.addEventListener('canplay', function() {
        //media.start();              
        log('canplay');
    }, true);

    media.addEventListener('canplaythrough', function() {
        log('canplaythrough');
        if(puiPlaying){
            media.play();
        }
    }, true);

    media.addEventListener('durationchange', function() {
        log('durationchange');
    }, true);

    media.addEventListener('ended', function() {
        // media.pause();              
        log('ended');
    }, true);

    media.addEventListener('error', function() {
        // media.pause();              
        log('error');
        log("media error:" + media.error.message);
        //  log("Error " + media.error.code + "; details: " + media.error.message);

    }, true);

    media.onerror = function() {
        log("Error " + media.error.message);
        log("Error " + media.error.code + "; details: " + media.error.message);

    }

    media.addEventListener('loadeddata', function() {
        // media.pause();              
        log('loadeddata');
    }, true);

    media.addEventListener('ended', function() {
        // media.pause();              
        log('ended');
    }, true);

    media.addEventListener('loadedmetadata', function() {
        // media.pause();              
        log('loadedmetadata');
        updatemetadata();
    }, true);

    media.addEventListener('loadstart', function() {
        // media.pause();              
        log('loadstart');
    }, true);

    media.addEventListener('durationchanged', function() {
        // media.pause();              
        console.log('durationchanged');
    }, true);

    // when unplugging ethernet cable, or when icecast server does not have the mount
    media.addEventListener('stalled', function() {
        // media.pause();              
        console.log('stalled');
        media.load();
    }, true);

    media.addEventListener('suspend', function() {
        // media.pause();              
        console.log('################suspend');
        // media.load();
    }, true);

    media.addEventListener('timeupdate', function() {
        // media.pause();              
        //console.log('timeupdate last:' + ltime + 'current: ' + media.currentTime);

        ltime = media.currentTime;

    }, true);

    media.addEventListener('progress', function() {
        // media.pause();              
        // console.log('progress:');
        // console.log(media.buffered.toString());
    }, true);

    window.setInterval(checkstream, 2000);

    log(navigator.userAgent);
    log(navigator.browserSpecs);
    log(navigator.mediaCapabilities);
    log(JSON.stringify(navigator));

    checktypes();
    checkstream();

    fixcache();
    //audio.load();
    // audio.play();
}

window.addEventListener("DOMContentLoaded", function() {
    whenLoaded();
}, false);

//document.onload(whenLoaded());
