---
layout: post
title: "Krakeling interview dave"
date: 2019-02-28 20:00:00+1
file: Dave.mp3
#file: test.mp3
summary: "Krakeling"
description: "blabla"
keywords: "krakeling,papillon"
voices: "?"
---

### Krakeling dave

We all know it, bullshit is also an integral part of squatting culture. Where are you Dave?
The krakpand is all prepared and ready. The sun is shining and there's not a better spot in Zeist to be than the Krakeling. Wanna join the occupation? Pay them a visit!



Een groep krakers is het zat eindeloos ontruimd te worden voor leegstand en heeft daarom besloten in hun pand te blijven aan de Krakelingweg 19 te Zeist.

De eigenaar van het pand wil het gebouw slopen, maar heeft hier nog geen vergunningen voor aangevraagd en verwacht zelf dat dit zeker nog een jaar gaat duren. Ook liet hij weten dat er geen bewoning in de vorm van anti-kraak kan plaatsvinden.

In eerste instantie liet de eigenaar de groep weten geïnteresseerd te zijn in de bruikleenovereenkomst die de bewoners voorstelden en was hierover met ze in gesprek. Toch heeft hij later, waarschijnlijk onder druk van het OM aangifte gedaan en een anti-kraak bedrijf gecontacteerd. Dit is voor de politie genoeg reden om het pand te ontruimen en de bewoners op straat te zetten. Zodat zij zelf dit leegstaande pand ook weer kunnen gebruiken voor trainingen van het Arrestatatie-team.

Het is nu de zoveelste keer op rij dat de groep hun huis uit wordt gezet voor anti-kraak; in feite gelegaliseerde en verkapte vorm van leegstand. Dit terwijl de politie aangeeft ‘niet te ontruimen voor leegstand’. Het lijkt erop dat het OM vooral bezig is de wet kraken en leegstand te hanteren om deze subcultuur de kop in te drukken, en niet om een einde te maken aan (onnodige) leegstand en woningsnood.

De bewoners van de Krakelingweg 19 vinden het onaceptabel dat er binnen de provincie Utrecht nauwelijks ruimte wordt gegeven aan alternatieve manieren van leven en sociale activiteiten die georganiseerd worden op autonome plekken. Hierom geeft de groep aan niet meer te vertrekken voor leegstand en wil van dit voormalige boedhistische centrum een mooie sociale en culturele plek maken.


