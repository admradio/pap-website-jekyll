---
layout: post
title: "leftover radio from lag: Teket, Survival System, Degrader, Modular Synth Jam (Negative Glitch, Franscore, random leftover ppl) "
date: 2019-12-05 21:00:34+01:00
file: 2019-12-05_20-46-19.mp4
audiofile: 2019-12-05-leftover-from-lag-liveset-night.flac
videofile: 2019-12-05_20-46-19.mp4
summary: " leftover radio from the lag"
description: " leftover radio from the lag"
keywords: "radio"
voices: "various"
times:
  - time: 00:00:01
    title: lalalaa
  - time: 00:00:02
    title: lala2

---

### 

- Teket
- Survival System
- Degrader
- Modular Synth Jam (Negative Glitch, Franscore, random leftover ppl)


### Links: 

- [Teket](https://soundcloud.com/total_reset)
- [Survival System](https://soundcloud.com/user-682613989)
- [Degrader](https://soundcloud.com/degrader81)
- [Negative Glitch](https://soundcloud.com/negativeglitch)
- [leftover](https://leftover.puscii.nl/)
