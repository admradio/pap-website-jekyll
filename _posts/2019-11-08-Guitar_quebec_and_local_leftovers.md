---
layout: post
title: "leftover from lag:Francois Levesque, proj, Negative glitch, degrader"
date: 2019-11-07 20:47:15+01:00
file: LAGThursday07.11.2019_guitar_from_quebec.ogg
summary: ""
description: " Francois Levesque, proj, Negative glitch, degrader"
keywords: "radio"
voices: "various"
---

### 

* Francois Levesque
* proj
* Negative Glitch
* Degrader

Visuals by andre-o

### Links: 
- [negative glitch](https://soundcloud.com/negativeglitch)
- [Francois Levesque](https://soundcloud.com/francoislevesque/sets/demo-2019)
- [Degrader](https://soundcloud.com/degrader81)
- [leftover](https://leftover.puscii.nl/)
