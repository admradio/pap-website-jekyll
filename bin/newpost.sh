#!/usr/bin/env bash 
TL=$(git rev-parse --show-toplevel)
DATETIME=`date --rfc-3339=seconds`
DATE=`date --rfc-3339=date`
FILE="put_filename_here"
TITLE="$1"
URL=https://leftover.puscii.nl/archive/
CLEANTITLE=$(echo $TITLE | sed -e 's/[^A-Za-z0-9._-]/_/g')

if [[ "$TITLE" = '' ]]
then
  echo 'makes new post'
  echo 'usage: ./newpost.sh [title]'
  exit 10
fi
 
echo "type a description"
read $DESCRIPTION

cat > $TL/_posts/$DATE-$CLEANTITLE.md << EOF
---
layout: post
title: "$TITLE"
date: $DATETIME
file: $FILE
summary: "$DESCRIPTION"
description: "$DESCRIPTION"
keywords: "radio"
voices: "various"
---

### $2

$DESCRIPTION

### Links: 

- [cateradio](https://cateradio.puscii.nl/)
EOF

git add $TL/_posts/$DATE-$CLEANTITLE.md
